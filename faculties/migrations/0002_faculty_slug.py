# Generated by Django 3.2.6 on 2021-08-21 10:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('faculties', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='faculty',
            name='slug',
            field=models.SlugField(blank=True, max_length=100),
        ),
    ]
