from django.shortcuts import render

from books.models import Book
from faculties.models import Faculty


def get_faculty_detail(request, slug):
    faculty = Faculty.objects.get(slug=slug)
    books = Book.objects.filter(faculty=faculty).all()
    return render(request, "faculties/faculty_detail.html", context={
        "faculty": faculty,
        "books": books,
    })
