from django.urls import path

from faculties import views

urlpatterns = [
    path("<slug>", views.get_faculty_detail, name="faculty_detail"),
]