from django.urls import path

from books import views


urlpatterns = [
    path("create", views.create_book, name="book_create"),
    path("<int:pk>", views.get_book_detail, name="book_detail"),
]
