from faculties.models import Faculty


def get_faculties(request):
    return {
        "faculties": Faculty.objects.all(),
    }
