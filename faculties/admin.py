from django.contrib import admin

from faculties.models import Faculty, Department


class FacultyAdmin(admin.ModelAdmin):
    pass


class DepartmentAdmin(admin.ModelAdmin):
    pass


admin.site.register(Faculty, FacultyAdmin)
admin.site.register(Department, DepartmentAdmin)
