from django.http.request import HttpRequest
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from books.models import Book
from books.forms import BookForm


def get_book_detail(request, pk):
    book = Book.objects.get(pk=pk)
    return render(request, "books/book_detail.html", context={
        "book": book
    })


@login_required
def create_book(request: HttpRequest):
    if request.method == "POST":
        form = BookForm(request.POST, request.FILES)
        if form.is_valid():
            book = form.save(commit=False)
            book.user = request.user
            book.save()
            return redirect("book_detail", pk=book.pk)
    else:
        form = BookForm()

    return render(request, "books/book_form.html", context={
        "form": form
    })
