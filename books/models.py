from django.db import models
from django.contrib.auth.models import User


class Book(models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    faculty = models.ForeignKey('faculties.Faculty', null=False, blank=False, related_name="books", on_delete=models.CASCADE)
    department = models.ForeignKey('faculties.Department', null=True, blank=True, related_name="books", on_delete=models.SET_NULL)
    price = models.DecimalField(max_digits=16, decimal_places=2, null=False, blank=False)
    cover = models.ImageField(upload_to="books", null=True, blank=True)
    user = models.ForeignKey(User, null=False, blank=False, related_name="books", on_delete=models.CASCADE)

    def __str__(self):
        return self.title
