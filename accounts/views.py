from django.shortcuts import render, redirect
from django.contrib.auth.models import User

from accounts.forms import RegisterForm


def register_user(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = User(
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email'],
                first_name=form.cleaned_data['first_name'],
                last_name=form.cleaned_data['last_name']
            )
            user.set_password(form.cleaned_data['password'])
            user.save()
            return redirect("/")
    else:
        form = RegisterForm()
    
    return render(request, "registration/register.html", context={
        "form": form
    })
